﻿using System.Collections.Generic;
using UnityEngine;

public class GameInit : MonoBehaviour
{

    GridEventDispatcher gridEventDispatcher;
    MenuEventDispatcher menuEventDispatcher;
    PopupEventDispatcher popupEventDispatcher;

    GridEventHandler gridEventHandler;
    MenuEventHandler menuEventHandler;
    PopupEventHandler popupEventHandler;

    public StructureSpawner spawner;
    public MenuController menu;
    public GridController grid;
    public PopupController popup;

    StructureDataArray structures;
    IFileIO fileIO;



    void Awake()
    {

        gridEventDispatcher = new GridEventDispatcher();
        menuEventDispatcher = new MenuEventDispatcher();
        popupEventDispatcher = new PopupEventDispatcher();

        fileIO = ServiceLocator.FileIO;
        FilePaths.Init();

        grid.Init();
        grid.MouseClickedFunction = MouseClicked;
        grid.EventDispatcher = gridEventDispatcher;

        popup.EventDispatcher = popupEventDispatcher;

        LoadStructures();
        BuildMenu();
        LoadPopups();
        LoadGrid();

        gridEventHandler = new GridEventHandler(gridEventDispatcher, menu, grid, spawner, popup);
        menuEventHandler = new MenuEventHandler(menuEventDispatcher, grid);
        popupEventHandler = new PopupEventHandler(popupEventDispatcher, grid);



    }

    void LoadStructures()
    {
        structures = fileIO.ReadObjectFromJson<StructureDataArray>(FilePaths.StructureFile);
    }

    void BuildMenu()
    {
        menu = new MenuController(menuEventDispatcher);
        menu.AddMenu(GameObject.FindGameObjectWithTag("LeftMenu").GetComponent<ExpandingMenu>());
        menu.AddMenu(GameObject.FindGameObjectWithTag("RightMenu").GetComponent<ExpandingMenu>());

        MenuDataArray menuData = fileIO.ReadObjectFromJson<MenuDataArray>(FilePaths.MenuFile);

        foreach (MenuData button in menuData.buttons)
        {
            StructureData structure = structures.GetById(button.objectId);
            menu.AddButton(button.menuIndex, button.x, button.y, structure.Color, structure);
        }
    }

    void LoadPopups()
    {
        popup.DataArray = fileIO.ReadObjectFromJson<PopupDataArray>(FilePaths.PopupFile);
        foreach(PopupData data in popup.DataArray.popups)
        {
            if (data.inheritColor)
            {
                data.color = structures.GetById(data.triggerIds[0]).color;
            }
        }
    }

    void LoadGrid()
    {
        List<PlacementData> placements = fileIO.ReadListFromFile<PlacementData>(FilePaths.GridFile);

        if (placements != null)
        {
            foreach (PlacementData placement in placements)
            {
                StructureData data = structures.GetById(placement.Id);
                Vector3 position = grid.Cursor.Get3dPositionFromGrid(placement.X, placement.Y, data.size);
                StructureController structure = spawner.Spawn(structures.GetById(placement.Id), position);
                grid.PlaceObject(structure, placement.Id, placement.X, placement.Y, data.size);
            }
        }

    }


    bool MouseClicked()
    {
        return Input.GetMouseButtonDown(0);
    }

}