﻿using UnityEngine;
class FilePaths
{
    public static string GridFile;
    public static string StructureFile;
    public static string MenuFile;
    public static string PopupFile;

    public static void Init()
    {
        GridFile = Application.dataPath + "/Save/grid.dat";
        StructureFile = Application.dataPath + "/Json/structures.json";
        MenuFile = Application.dataPath + "/Json/menu.json";
        PopupFile = Application.dataPath + "/Json/popups.json";
    }

}

