﻿using UnityEngine;

public class GridEventHandler
{
    GridEventDispatcher eventDispatcher;
    MenuController menu;
    GridController grid;
    StructureSpawner spawner;
    PopupController popup;

    public GridEventHandler(
        GridEventDispatcher eventDispatcher, 
        MenuController menu, 
        GridController grid, 
        StructureSpawner spawner,
        PopupController popup)
    {
        this.eventDispatcher = eventDispatcher;
        this.menu = menu;
        this.grid = grid;
        this.spawner = spawner;
        this.popup = popup;

        SubscribeToEvents();
    }


    void OnMouseOver(object target)
    {
        StructureController structure = (StructureController)target;
        structure.ToggleHighlight(true);
    }

    void OnMouseOut(object target)
    {
        StructureController structure = (StructureController)target;
        structure.ToggleHighlight(false);
    }

    void OnClickObject(object target)
    {
        StructureController structure = (StructureController)target;
        popup.Show(structure.Id);
    }

    void OnPlaceObject(Vector3 position)
    {
        StructureData structureData = (StructureData)menu.SelectedObject;

        if (!(structureData.IsUnique && grid.IsPlaced(structureData.Id)))
        {
            StructureController structure = spawner.Spawn(structureData, position);
            grid.PlaceObject(structure, structure.Id);
            ServiceLocator.FileIO.WriteListToFile(grid.Data, FilePaths.GridFile);
        }

    }

    
    void SubscribeToEvents()
    {
        eventDispatcher.MouseOverEvent += OnMouseOver;
        eventDispatcher.MouseOutEvent += OnMouseOut;
        eventDispatcher.ClickObjectEvent += OnClickObject;
        eventDispatcher.PlaceObjectEvent += OnPlaceObject;
    }
}

