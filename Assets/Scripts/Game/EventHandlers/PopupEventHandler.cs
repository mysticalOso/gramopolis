﻿public class PopupEventHandler
{
    PopupEventDispatcher eventDispatcher;
    GridController grid;

    public PopupEventHandler(PopupEventDispatcher eventDispatcher, GridController grid)
    {
        this.eventDispatcher = eventDispatcher;
        this.grid = grid;
        SubscribeToEvents();
    }

    void OnOpen()
    {
        grid.enabled = false;
    }

    void OnClose()
    {
        grid.enabled = true;
    }

    void SubscribeToEvents()
    {
        eventDispatcher.OpenEvent += OnOpen;
        eventDispatcher.CloseEvent += OnClose;
    }
}

