﻿
public class MenuEventHandler
{
    MenuEventDispatcher eventDispatcher;
    GridCursor cursor;
    GridController grid;

    public MenuEventHandler(MenuEventDispatcher eventDispatcher, GridController grid)
    {
        this.eventDispatcher = eventDispatcher;
        this.grid = grid;
        cursor = grid.Cursor;
        SubscribeToEvents();
    }

    void OnOpen(object linkedObject)
    {
        grid.enabled = false;
    }

    void OnClose(object linkedObject)
    {
        grid.enabled = true;
        StructureData structureData = (StructureData)linkedObject;
        cursor.Color = structureData.Color;
        cursor.Size = structureData.Size;
    }

    void SubscribeToEvents()
    {
        eventDispatcher.OpenEvent += OnOpen;
        eventDispatcher.CloseEvent += OnClose;
    }

}

