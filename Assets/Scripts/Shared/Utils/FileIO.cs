﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class FileIO : IFileIO
{
    public string ReadStringFromFile(string url)
    {
        StreamReader sr = new StreamReader(url);
        return sr.ReadToEnd();
    }

    public T ReadObjectFromJson<T>(string url)
    {
        string jsonString = ReadStringFromFile(url);
        return JsonUtility.FromJson<T>(jsonString);
    }

    public List<T> ReadListFromFile<T>(string url)
    {
        List<T> list = null;
        FileInfo fileInfo = new FileInfo(url);

        if (fileInfo.Exists)
        {
            FileStream binaryFile = fileInfo.OpenRead();
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            list = (List<T>)binaryFormatter.Deserialize(binaryFile);
            binaryFile.Close();
        }

        return list;
    }

    public void WriteListToFile<T>(List<T> list, string url)
    {
        FileInfo fileInfo = new FileInfo(url);
        FileStream binaryFile = fileInfo.Create();
        BinaryFormatter binaryFormatter = new BinaryFormatter();

        binaryFormatter.Serialize(binaryFile, list);
        binaryFile.Flush();
        binaryFile.Close();
        
    }
}

