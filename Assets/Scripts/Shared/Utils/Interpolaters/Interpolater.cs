﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interpolater : MonoBehaviour
{
    float elapsedTime;
    float duration;
    Action onFinished;
    Action<float> onUpdate;

    public void Interpolate(float duration, Action<float> onUpdate, Action onFinished)
    {
        this.onFinished = onFinished;
        this.onUpdate = onUpdate;
        this.duration = duration;
        elapsedTime = 0;
        StartCoroutine(UpdateInterolation());
    }


    IEnumerator UpdateInterolation()
    {

        do
        {
            elapsedTime += Time.deltaTime;
            float ratio = EaseOutSine(0, 1, elapsedTime, duration);
            onUpdate(ratio);
            yield return 0;
        } while (elapsedTime < duration);

        onUpdate(1);
        if (onFinished != null) onFinished();
    }


    float EaseOutSine(float start, float distance, float elapsedTime, float duration)
    {
        if (elapsedTime > duration) { elapsedTime = duration; }
        return distance * Mathf.Sin(elapsedTime / duration * (Mathf.PI / 2)) + start;
    }


}
