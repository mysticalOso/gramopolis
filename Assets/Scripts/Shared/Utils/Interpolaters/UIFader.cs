﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

//Fades all graphics in object and children
public class UIFader : Interpolater
{
    List<Graphic> graphics;
    List<float> startAlphas;
    List<float> deltas;

    public void Awake()
    {
        graphics = new List<Graphic>(GetComponentsInChildren<Graphic>());
    }

    public void FadeToTarget(float targetAlpha, float duration, Action fadeFinished)
    {
        startAlphas = new List<float>();
        deltas = new List<float>();
        foreach(Graphic graphic in graphics)
        {
            startAlphas.Add(graphic.color.a);
            deltas.Add(targetAlpha - graphic.color.a);
        }

        Interpolate(duration, UpdateFade, fadeFinished);
    }


    void UpdateFade(float ratio)
    {
        for (int i = 0; i < graphics.Count; i++)
        {
            Color color = graphics[i].color;
            color.a = startAlphas[i] + ratio * deltas[i];
            graphics[i].color = color;
        }
    }


}

