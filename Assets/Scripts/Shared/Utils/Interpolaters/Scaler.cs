﻿using System;
using UnityEngine;

//Uniform scaler assumes scale equal in all dimensions
public class Scaler : Interpolater
{ 
    float startScale;
    float delta;

    public void ScaleToTarget(float targetScale, float duration, Action moveFinished)
    {
        startScale = transform.localScale.x;
        delta = targetScale - startScale;
        Interpolate(duration, UpdateMove, moveFinished);
    }


    void UpdateMove(float ratio)
    {
        float scale = startScale + ratio * delta;
        transform.localScale = new Vector3(scale, scale, scale);
    }
}
