﻿using System;
using UnityEngine;

public class Mover : Interpolater
{
    Vector2 startPos;
    Vector2 delta;

    public void MoveToTarget(Vector2 target, float duration, Action moveFinished)
    {
        startPos = transform.localPosition;
        delta = target - startPos;
        Interpolate(duration, UpdateMove, moveFinished);
    }


    void UpdateMove(float ratio)
    {
        transform.localPosition = startPos + ratio * delta;
    }



}
