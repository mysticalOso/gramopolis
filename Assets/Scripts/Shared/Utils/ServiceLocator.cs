﻿//Allows FileIO to be accessible anywhere but also easily replaced
//if system specific implementation was required
class ServiceLocator
{
    static IFileIO fileIO = new FileIO();

    public static IFileIO FileIO
    {
        get { return fileIO; }
        set { fileIO = value; }
    }
            
}

