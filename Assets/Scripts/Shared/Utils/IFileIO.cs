﻿using System.Collections.Generic;

public interface IFileIO
{
    string ReadStringFromFile(string url);
    T ReadObjectFromJson<T>(string url);
    List<T> ReadListFromFile<T>(string url);
    void WriteListToFile<T>(List<T> list, string url);
}