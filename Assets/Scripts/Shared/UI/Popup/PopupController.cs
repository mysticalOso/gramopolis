﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PopupController : MonoBehaviour
{
    UIFader fader;
    Scaler scaler;
    TextTyper typer;

    PopupDataArray dataArray;
    Image background;
    Text textBox;

    float timeToClose = 3;

    string text;

    PopupEventDispatcher eventDispatcher;

    void Start()
    {
        fader = GetComponent<UIFader>();
        scaler = GetComponent<Scaler>();
        typer = GetComponentInChildren<TextTyper>();
        textBox = GetComponentInChildren<Text>();
        background = GetComponent<Image>();
    }

    public void Show(int triggerId)
    {
        eventDispatcher.FireOpenEvent();

        PopupData data = dataArray.GetByTriggerId(triggerId);
        background.color = data.Color;
        textBox.color = data.TextColor;
        textBox.text = "";
        text = data.text;

        fader.FadeToTarget(1, 0.5f, null);
        scaler.ScaleToTarget(1, 0.5f, TypeText);
    }

    public void Hide()
    {
        fader.FadeToTarget(0, 0.5f, null);
        scaler.ScaleToTarget(0, 0.5f, Closed);
    }

    void Closed()
    {
        eventDispatcher.FireCloseEvent();
    }

	void TypeText ()
    {
        typer.TypeText(text);
        StartCoroutine(CloseCountdown());
	}

    IEnumerator CloseCountdown()
    {
        yield return new WaitForSeconds(timeToClose);
        Hide();
    }

    public PopupDataArray DataArray
    {
        set { dataArray = value; }
        get { return dataArray; }
    }

    public PopupEventDispatcher EventDispatcher
    {
        set { eventDispatcher = value; }
    }
}
