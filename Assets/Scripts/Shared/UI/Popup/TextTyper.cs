﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextTyper : MonoBehaviour {

    public string textToType;
    Text textBox;
    int cursor = 0;

    public float typeDelay = 0.1f;

    void Start()
    {
        textBox = GetComponent<Text>();
    }

    public void TypeText(string text)
    {
        textToType = text;
        cursor = 0;
        StartCoroutine(UpdateTyping());
    }

    IEnumerator UpdateTyping()
    {
        while (cursor < textToType.Length)
        {
            cursor++;
            textBox.text = textToType.Substring(0, cursor);
            yield return new WaitForSeconds(typeDelay);
        }
    }

    public void ClearText()
    {
        textBox.text = "";
    }
}
