﻿using System;
using UnityEngine;

[Serializable]
public class PopupData
{
    public int[] triggerIds;
    public bool inheritColor = true;
    public float[] color;
    public float[] textColor;
    public string text;

    public Color Color
    {
        get { return new Color(color[0], color[1], color[2]); }
    }

    public Color TextColor
    {
        get { return new Color(textColor[0], textColor[1], textColor[2]); }
    }
}

