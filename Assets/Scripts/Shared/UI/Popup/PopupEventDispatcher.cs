﻿using System;

public class PopupEventDispatcher
{
    public event Action CloseEvent;
    public event Action OpenEvent;

    public void FireOpenEvent()
    {
        if (OpenEvent != null) OpenEvent();
    }

    public void FireCloseEvent()
    {
        if (CloseEvent != null) CloseEvent();
    }

}

