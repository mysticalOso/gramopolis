﻿using System;
using System.Collections.Generic;

public class PopupDataArray
{
    public PopupData[] popups;

    Dictionary<int, PopupData> dictionary = null;

    public PopupData GetByTriggerId(int id)
    {
        if (dictionary == null) InitDictionary();
        return dictionary[id];
    }

    void InitDictionary()
    {
        dictionary = new Dictionary<int, PopupData>();
        foreach (PopupData popup in popups)
        {
            foreach(int id in popup.triggerIds)
            {
                dictionary.Add(id, popup);
            }
            
        }
    }
}

