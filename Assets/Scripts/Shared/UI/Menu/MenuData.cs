﻿using System;

[Serializable]
public class MenuData
{
    public int menuIndex;
    public int x;
    public int y;
    public int objectId;
}

