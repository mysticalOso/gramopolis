﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExpandingMenu : MonoBehaviour {

    public ExpandingMenuButton MenuButtonPrefab;
    public Vector2 ExpandDirection;

    MenuEventDispatcher eventDispatcher;

    public ExpandingMenuButton AddButton(int x, int y)
    {
        ExpandingMenuButton button = Instantiate(MenuButtonPrefab);
        button.transform.SetParent(transform,false);
        button.transform.localPosition = new Vector3(0, 0, 0);
        button.ExpandTarget = new Vector2(ExpandDirection.x * x, ExpandDirection.y * y);
        button.EventDispatcher = eventDispatcher;
        return button;
    }

    public MenuEventDispatcher EventDispatcher
    {
        set { eventDispatcher = value; }
    }

}
