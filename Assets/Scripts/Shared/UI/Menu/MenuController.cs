﻿using System.Collections.Generic;
using UnityEngine;

public class MenuController
{
    List<ExpandingMenu> menus = new List<ExpandingMenu>();
    object selectedObject;
    MenuEventDispatcher eventDispatcher;

    public MenuController(MenuEventDispatcher eventDispatcher)
    {
        this.eventDispatcher = eventDispatcher;
        eventDispatcher.ClickedEvent += OnButtonClicked;
    }

    public void AddButton(int menuIndex, int x, int y, Color color, object linkedObject)
    {
        ExpandingMenuButton newButton = menus[menuIndex].AddButton(x,y);
        newButton.Color = color;
        newButton.LinkedObject = linkedObject;
        newButton.MenuIndex = menuIndex;
    }

    public void AddMenu(ExpandingMenu menu)
    {
        menu.EventDispatcher = eventDispatcher;
        menus.Add(menu);
    }

    public object SelectedObject
    {
        get { return selectedObject; }
    }

    void OnButtonClicked(ExpandingMenuButton button)
    {
        selectedObject = button.LinkedObject;
        if (!button.Expanded) eventDispatcher.FireOpenEvent(selectedObject);
        else eventDispatcher.FireCloseEvent(selectedObject);
    }



}

