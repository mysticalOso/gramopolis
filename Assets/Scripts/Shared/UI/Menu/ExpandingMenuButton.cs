﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ExpandingMenuButton : MonoBehaviour, IPointerClickHandler
{

    bool moving = false;
    bool expanded = false;
    bool selected = false;
    Mover mover;

    Vector2 expandTarget;
    int menuIndex;

    object linkedObject;
    Action<object> onClickAction;

    MenuEventDispatcher dispatcher;

    public void Start()
    {
        mover = GetComponent<Mover>();
    }


    private void OnAnyButtonClicked(ExpandingMenuButton button)
    {
        selected = (button == this);
        if (button.MenuIndex == menuIndex || expanded)
        {
            ToggleExpand();
        }
        
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!moving)
        {
            transform.SetAsLastSibling();
            dispatcher.FireClickedEvent(this);
        }
    }


    public void ToggleExpand()
    {
        if (!expanded)
        {
            gameObject.SetActive(true);
            mover.MoveToTarget(expandTarget, 0.5f, ExpandFinished);
            expanded = true;
        }
        else
        {
            mover.MoveToTarget(new Vector2(0,0), 0.5f, ContractFinished);
            expanded = false;
        }
        moving = true;
    }

    public void ExpandFinished()
    {
        moving = false;
    }

    public void ContractFinished()
    {
        moving = false;
       // if (!selected) gameObject.SetActive(false);
    }





    public bool Expanded
    {
        get { return expanded; }
    }

    public Vector2 ExpandTarget
    {
        set { expandTarget = value; }
    }

    public object LinkedObject
    {
        set { linkedObject = value; }
        get { return linkedObject; }
    }

    public int MenuIndex
    {
        set { menuIndex = value; }
        get { return menuIndex; }
    }

    public Color Color
    {
        set { GetComponent<Image>().color = value; }
    }

    public Action<object> OnClickAction
    {
        set { onClickAction = value; }
    }

    public MenuEventDispatcher EventDispatcher
    {
        set
        {
            dispatcher = value;
            dispatcher.ClickedEvent += OnAnyButtonClicked;
        }
    }
}
