﻿using System;

public class MenuEventDispatcher
{
    public event Action<object> CloseEvent;
    public event Action<object> OpenEvent;
    public event Action<ExpandingMenuButton> ClickedEvent;

    public void FireOpenEvent(object linkedObject)
    {
        if (OpenEvent != null) OpenEvent(linkedObject);
    }

    public void FireCloseEvent(object linkedObject)
    {
        if (CloseEvent != null) CloseEvent(linkedObject);
    }

    public void FireClickedEvent(ExpandingMenuButton sender)
    {
        if (ClickedEvent != null) ClickedEvent(sender);
    }
}

