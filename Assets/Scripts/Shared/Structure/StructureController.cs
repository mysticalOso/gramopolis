﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StructureController : MonoBehaviour
{
    StructureData data;
    MeshAnimator meshAnim;
    SegmentedMesh mesh;
    Material material;

    float time = 0;
    float scale;

    void Start ()
    {
        mesh = new SegmentedMesh(GetComponent<MeshFilter>().mesh);
        
        material = GetComponent<MeshRenderer>().material;
        meshAnim = GetComponent<MeshAnimator>();
        InitFromData();
        GetComponent<Scaler>().ScaleToTarget(scale, 1, null);
    }


    public void InitFromData()
    {
        meshAnim.Data = data.modifiers;
        meshAnim.isStatic = data.isStatic;
        meshAnim.phase = data.phase;
        material.color = data.Color;
        scale = data.Size*0.5f;
    }


    public void ToggleHighlight(bool isOn)
    {
        
        material.EnableKeyword("_EmissionColor");
        if (isOn)
        {
            material.SetColor("_EmissionColor", new Color(0.2f, 0.2f, 0.2f));
        }
        else
        {
            material.SetColor("_EmissionColor", new Color(0, 0, 0));
        }
    }


    public StructureData Data
    {
        set { data = value; }
    }

    public int Id
    {
        get { return data.Id; }
    }

    public bool IsUnique
    {
        get { return data.IsUnique; }
    }
}
