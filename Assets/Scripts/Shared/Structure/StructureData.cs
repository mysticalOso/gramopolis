﻿using System;
using UnityEngine;

[Serializable]
public class StructureData
{
    public int id;
    public float[] color;
    public bool isUnique = true;
    public int size = 2;
    public float phase = 0;
    public bool isStatic = false;
    public MeshModifierData[] modifiers = new MeshModifierData[0];

    public Color Color
    {
        get { return new Color(color[0], color[1], color[2]); }
    }
    public int Id
    {
        get { return id; }
    }

    public int Size
    {
        get { return size; }
    }

    public bool IsUnique
    {
        get { return isUnique; }
    }
}
