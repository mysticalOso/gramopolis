﻿using System;
using System.Collections.Generic;

[Serializable]
public class StructureDataArray
{
    public StructureData[] structures;

    Dictionary<int, StructureData> dictionary = null;

    public StructureData GetById(int id)
    {
        if (dictionary == null) InitDictionary();
        return dictionary[id];
    }

    void InitDictionary()
    {
        dictionary = new Dictionary<int, StructureData>();
        foreach (StructureData structure in structures)
        {
            dictionary.Add(structure.Id, structure);
        }
    }
}
