﻿using UnityEngine;

public class StructureSpawner : MonoBehaviour
{
    public StructureController structurePrefab;

    public StructureController Spawn(object data, Vector3 position)
    {
        StructureData structureData = (StructureData)data;
        StructureController structure = Instantiate(structurePrefab);
        structure.Data = structureData;
        structure.transform.localPosition = position;
        return structure;
    }
}