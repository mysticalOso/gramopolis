﻿using UnityEngine;
using System.Collections.Generic;


public class SegmentedMesh
{
    public Vector3 size = new Vector3(1, 0.1f, 1);

    public int segmentCount = 20;

    List<Vector3> originalVerts = new List<Vector3>();
    List<Vector3> verts = new List<Vector3>();
    List<int> indices = new List<int>();

    Mesh mesh;

    public SegmentedMesh(Mesh mesh)
    {
        this.mesh = mesh;

        AddBase();
        AddSegments();
        AddTop();

        SaveOriginalVerts();

        UpdateMesh();
    }



    void AddBase()
    {
        AddBaseVerts();
        AddBaseFace();
    }

    void AddBaseVerts()
    {
        Vector3 halfSize = size * 0.5f;

        Vector3 p0 = new Vector3(-halfSize.x, -halfSize.y, -halfSize.z);
        Vector3 p1 = new Vector3(-halfSize.x, -halfSize.y, halfSize.z);
        Vector3 p2 = new Vector3(halfSize.x, -halfSize.y, halfSize.z);
        Vector3 p3 = new Vector3(halfSize.x, -halfSize.y, -halfSize.z);

        Vector3[] vertArray = new Vector3[]{
             p0,p1,p2,p3,
             p0,p1,p1,p2,p2,p3,p3,p0
         };

        verts = new List<Vector3>(vertArray);
    }

    void AddBaseFace()
    {
        indices.Add(0);
        indices.Add(3);
        indices.Add(1);
        indices.Add(3);
        indices.Add(2);
        indices.Add(1);
    }



    void AddSegments()
    {
        for (int i=0; i<segmentCount; i++)
        {
            AddSegmentVerts(i);
            AddSideFaces(i);
        }
    }

    void AddSegmentVerts(int segmentIndex)
    {
        for (int i = 4; i <12; i++)
        {
            Vector3 vert = verts[i];
            vert.y += size.y * (segmentIndex+1);
            verts.Add(vert);
        }
    }

    void AddSideFaces(int segmentIndex)
    {
        int i = verts.Count - 16; //vert index
        for (int n = 0; n < 4; n++)
        {
            indices.Add(i);
            indices.Add(i+1);
            indices.Add(i+8);
            indices.Add(i+1);
            indices.Add(i+9);
            indices.Add(i+8);

            i += 2;
        }
    }



    void AddTop()
    {
        AddTopVerts();
        AddTopFace();
    }

    void AddTopVerts()
    {
        for (int i = 0; i < 4; i++)
        {
            Vector3 vert = verts[i];
            vert.y += size.y * segmentCount;
            verts.Add(vert);
        }
    }

    void AddTopFace()
    {
        int i = verts.Count - 4;

        indices.Add(i);
        indices.Add(i + 1);
        indices.Add(i + 2);
        indices.Add(i);
        indices.Add(i + 2);
        indices.Add(i + 3);
    }



    public void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = verts.ToArray();
        mesh.triangles = indices.ToArray();
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }

    public void ModifySegment(int segmentIndex, MeshModifier modifier)
    {
        int startIndex = GetSegmentStartIndex(segmentIndex);
        int vertCount = GetSegmentVertCount(segmentIndex);

        List<Vector3> segment = verts.GetRange(startIndex, vertCount);

        segment = modifier.ModifyVerts(segment);

        for (int i = startIndex; i<(startIndex+vertCount); i++)
        {
            verts[i] = segment[i - startIndex];
        }
    }


    void SaveOriginalVerts()
    {
        foreach(Vector3 v in verts)
        {
            originalVerts.Add(v);
        }
    }

    public void ResetVerts()
    {
        for (int i = 0; i < originalVerts.Count; i++)
        {
            verts[i] = originalVerts[i];
        }
    }


    int GetSegmentStartIndex(int i)
    {
        if (i == 0) return 0;
        else
        {
            return (i * 8) + 4;
        }
    }

    int GetSegmentVertCount(int i)
    {
        if (i == 0 || i == segmentCount)
        {
            return 12;
        }
        else return 8;
    }

}

