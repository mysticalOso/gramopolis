﻿using System;

[Serializable]
public class MeshModifierData
{
    public string name;
    public float strength;
}

