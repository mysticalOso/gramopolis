﻿using System.Collections.Generic;
using UnityEngine;

public class MeshModifier
{
    public float strength = 1;
    
    public virtual List<Vector3> ModifyVerts(List<Vector3> verts)
    {
        return null;
    }

    public virtual void ModifyMesh(SegmentedMesh mesh, float amount) { }

    protected Vector3 GetCentre(List<Vector3> verts)
    {
        Vector3 total = new Vector3();
        foreach(Vector3 vert in verts)
        {
            total += vert;
        }
        return total / verts.Count;
    }

    protected List<Vector3> ScaleVertsAboutPivot(List<Vector3> verts, Vector3 pivot, float scale)
    {
        for (int i = 0; i < verts.Count; i++)
        {
            verts[i] = ScaleVertAboutPivot(verts[i], pivot, scale);
        }
        return verts;
    }

    Vector3 ScaleVertAboutPivot(Vector3 vert, Vector3 pivot, float scale)
    {
        vert = pivot + (vert - pivot) * scale;
        return vert;
    }

    protected List<Vector3> RotateVertsAboutPivot(List<Vector3> verts, Vector3 pivot, Vector3 angles)
    {
        for (int i=0; i<verts.Count; i++)
        {
            verts[i] = RotateVertAboutPivot(verts[i], pivot, angles);
        }
        return verts; //TODO test if return is needed
    }

    Vector3 RotateVertAboutPivot(Vector3 vert, Vector3 pivot, Vector3 angles)
    {
        Vector3 dir = vert - pivot;
        dir = Quaternion.Euler(angles) * dir;
        vert = dir + pivot;
        return vert;
    }
}

