﻿
using System.Collections.Generic;
using UnityEngine;

public class MeshTwister : MeshModifier
{
    Vector3 angles;

    public override void ModifyMesh(SegmentedMesh mesh, float amount)
    {
        angles = new Vector3();
        for (int i=1; i<=mesh.segmentCount; i++)
        {
            angles.y += amount*strength;
            mesh.ModifySegment(i, this);
        }
    }

    public override List<Vector3> ModifyVerts(List<Vector3> verts)
    {
        Vector3 centre = GetCentre(verts);
        RotateVertsAboutPivot(verts, centre, angles);
        return verts;
    }
}

