﻿using System.Collections.Generic;
using UnityEngine;

public class MeshAnimator : MonoBehaviour
{
    public List<MeshModifier> modifiers = new List<MeshModifier>();

    public bool isStatic = false;
    public float phase = 0;

    float time;

    SegmentedMesh mesh;

    void Start()
    {
        mesh = new SegmentedMesh(GetComponent<MeshFilter>().mesh);
    }

    void Update()
    {
        mesh.ResetVerts();

        if (!isStatic) time += Time.deltaTime;
        float amount = Mathf.Sin(time * 0.5f + phase*Mathf.PI);

        foreach(MeshModifier modifier in modifiers)
        {
            modifier.ModifyMesh(mesh, amount);
        }

        mesh.UpdateMesh();
    }

    public MeshModifierData[] Data
    {
        set
        {
            foreach (MeshModifierData data in value)
            {
                MeshModifier modifier = null;
                if (data.name == "bender") modifier = new MeshBender();
                if (data.name == "twister") modifier = new MeshTwister();
                if (data.name == "waver") modifier = new MeshWaver();
                modifier.strength = data.strength;
                modifiers.Add(modifier);
            }
        }
    }



}

