﻿
using System.Collections.Generic;
using UnityEngine;

public class MeshBender : MeshModifier
{
    Vector3 angles;

    public override void ModifyMesh(SegmentedMesh mesh, float amount)
    {
        angles = new Vector3();
        for (int i=1; i<=mesh.segmentCount; i++)
        {
            angles.x += amount * strength;
            mesh.ModifySegment(i, this);
        }
    }

    public override List<Vector3> ModifyVerts(List<Vector3> verts)
    {
        Vector3 pivot = new Vector3(1.5f, 0.5f, 0);
        RotateVertsAboutPivot(verts, pivot, angles);
        return verts;
    }
}

