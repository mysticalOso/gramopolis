﻿
using System.Collections.Generic;
using UnityEngine;

public class MeshWaver : MeshModifier
{
    float scale;

    public override void ModifyMesh(SegmentedMesh mesh, float amount)
    {
        for (int i=1; i<=mesh.segmentCount; i++)
        {
            scale = 1 - (Mathf.Sin(i * strength * Mathf.PI*0.05f) * amount*0.5f);
            mesh.ModifySegment(i, this);
        }
    }

    public override List<Vector3> ModifyVerts(List<Vector3> verts)
    {
        Vector3 centre = GetCentre(verts);
        ScaleVertsAboutPivot(verts, centre, scale);
        return verts;
    }
}

