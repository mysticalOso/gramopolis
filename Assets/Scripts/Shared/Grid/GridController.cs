﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class GridController : MonoBehaviour
{
    public Vector2 gridSize = new Vector2(20,20);
    GridCursor cursor;
    PlacementGrid placementGrid;
    object mouseOverObject;

    Func<bool> mouseClickedFunction;

    GridEventDispatcher eventDispatcher;

    public void Init()
    {
        cursor = GetComponentInChildren<GridCursor>();
        placementGrid = new PlacementGrid((int)gridSize.x, (int)gridSize.y);
        enabled = false;
    }

    void Update()
    {
        cursor.UpdateCursor();

        if (cursor.Hit)
        {
            UpdateCursorHit();
        }
    }

    void UpdateCursorHit()
    {
        if (CursorIsOverObject())
        {
            cursor.IsVisible = false;
            if (MouseClicked)
            {
                eventDispatcher.FireClickObjectEvent(mouseOverObject);
            }
        }
        else if (placementGrid.CanPlace(cursor.GridX, cursor.GridY, cursor.Size))
        {
            cursor.IsVisible = true;
            if (MouseClicked)
            {
                eventDispatcher.FirePlaceObjectEvent(cursor.Position);
            }
        }
    }

    public void PlaceObject(object obj, int id)
    {
        placementGrid.Place(cursor.GridX, cursor.GridY, id, cursor.Size, obj);
    }

    public void PlaceObject(object obj, int id, int x, int y, int size)
    {
        placementGrid.Place(x, y, id, size, obj);
    }

    public bool IsPlaced(int id)
    {
        return placementGrid.IsPlaced(id);
    }


    bool CursorIsOverObject()
    {
        object oldMouseOverObject = mouseOverObject;
        mouseOverObject = placementGrid.GetObjectAt(cursor.GridMousePos);

        FireEventsIfOverObjectChanged(oldMouseOverObject);

        return (mouseOverObject!=null);
    }

    void FireEventsIfOverObjectChanged(object oldMouseOverObject)
    {
        if (mouseOverObject != oldMouseOverObject)
        {
            if (oldMouseOverObject != null)
            {
                eventDispatcher.FireMouseOutEvent(oldMouseOverObject);
            }
            if (mouseOverObject != null)
            {
                eventDispatcher.FireMouseOverEvent(mouseOverObject);
            }
        }
    }

    public List<PlacementData> Data
    {
        get { return placementGrid.DataList; }
    }

    bool MouseClicked
    {
        get { return mouseClickedFunction(); }
    }

    public Func<bool> MouseClickedFunction
    {
        set
        {
            mouseClickedFunction = value;
        }
    }

    public GridEventDispatcher EventDispatcher
    {
        set
        {
            eventDispatcher = value;
        }
    }

    public GridCursor Cursor
    {
        get
        {
            return GetComponentInChildren<GridCursor>();
        }
    }
}

