﻿using System;
using UnityEngine;


public class GridEventDispatcher
{
    public event Action<object> MouseOverEvent; 
    public event Action<object> MouseOutEvent;
    public event Action<object> ClickObjectEvent;
    public event Action<Vector3> PlaceObjectEvent;

    public void FireMouseOverEvent(object target)
    {
        if (MouseOverEvent != null) MouseOverEvent(target);
    }

    public void FireMouseOutEvent(object target)
    {
        if (MouseOverEvent != null) MouseOutEvent(target);
    }

    public void FireClickObjectEvent(object target)
    {
        if (ClickObjectEvent != null) ClickObjectEvent(target);
    }

    public void FirePlaceObjectEvent(Vector3 position)
    {
        if (PlaceObjectEvent != null) PlaceObjectEvent(position);
    }


}

