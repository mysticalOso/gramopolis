﻿using UnityEngine;

public class GridCursor : MonoBehaviour
{
    int cursorSize;
    float halfSize;

    Vector2 halfGridSize = new Vector2(10, 10);

    Vector2 gridOffset = new Vector2(10,10);

    Vector3 hitPos;
    bool cursorHit;

    new MeshRenderer renderer;

    void Start()
    {
        Size = 1;
        renderer = GetComponentInChildren<MeshRenderer>();
        IsVisible = false;
    }

    public void UpdateCursor()
    {
        Vector3 pos = transform.position;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            cursorHit = true;
            hitPos = hit.point;
            pos = SnapPosition(hitPos);
            IsVisible = true;
        }
        else
        {
            cursorHit = false;
            IsVisible = false;
        }

        if (pos != transform.position)
        {
            transform.position = pos;
        }
    }

    Vector3 SnapPosition(Vector3 pos)
    {
        pos.x = Mathf.Round(pos.x - halfSize) + halfSize;
        pos.z = Mathf.Round(pos.z - halfSize) + halfSize;

        if (pos.x > (halfGridSize.x-halfSize)) pos.x = (halfGridSize.x - halfSize);
        if (pos.x < (halfSize - halfGridSize.x)) pos.x = halfSize - halfGridSize.x;
        if (pos.z > (halfGridSize.y - halfSize)) pos.z = (halfGridSize.y - halfSize);
        if (pos.z < (halfSize- halfGridSize.y)) pos.z = halfSize - halfGridSize.y;

        return pos;
    }


    public Vector3 Get3dPositionFromGrid(int gridX, int gridY, int size)
    {
        float halfSize = size * 0.5f;
        float x = gridX - gridOffset.x + halfSize;
        float z = gridY - gridOffset.y + halfSize;
        return new Vector3(x, 0, z);
    }

    //Snapped 3d position used for 3d object position
    public Vector3 Position
    {
        get
        {
            Vector3 pos = transform.localPosition;
            //pos.y -= heightOffset;
            return pos;
        }
    }

    //position on 2d grid used for isOverObject
    public Vector2 GridMousePos
    {
        get { return (new Vector2(hitPos.x,hitPos.z) + gridOffset);}
    }


    //grid index used for placement test and storage
    public int GridX
    {
        get { return (int)(Position.x + gridOffset.x - halfSize); }
    }

    public int GridY
    {
        get { return (int)(Position.z + gridOffset.y - halfSize); }
    }





    public bool Hit
    {
        get { return cursorHit; }
    }

    public bool IsVisible
    {
        get
        {
            return renderer.enabled;
        }
        set
        {
            renderer.enabled = value;
        }
    }

    public Color Color
    {
        set
        {
            renderer.material.color = value;
        }
    }

    public int Size
    {
        set
        {
            cursorSize = value;
            halfSize = cursorSize * 0.5f;
            gameObject.transform.localScale = new Vector3(cursorSize, gameObject.transform.localScale.y, cursorSize);
        }
        get
        {
            return cursorSize;
        }
    }

}

