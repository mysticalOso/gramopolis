﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class PlacementGrid
{
    List<PlacementEntry> placements; 
    CollisionGrid collisionGrid;

    public PlacementGrid(int xSize, int ySize)
    {
        placements = new List<PlacementEntry>();
        collisionGrid = new CollisionGrid(xSize, ySize);
    }

    public bool Place(int x, int y, int id, int size, object linkedObject)
    {
        if (collisionGrid.Place(x, y, size))
        {
            placements.Add(new PlacementEntry(id,x,y,size,linkedObject));
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CanPlace(int x, int y, int size)
    {
        return collisionGrid.CanPlace(x, y, size);
    }


    public object GetObjectAt(Vector2 pos)
    {
        foreach(PlacementEntry placement in placements)
        {
            if (placement.IsTouching(pos)) return placement.LinkedObject;
        }
        return null;
    }

    public bool IsPlaced(int id)
    {
        foreach (PlacementEntry placement in placements)
        {
            if (placement.Id == id) return true;
        }
        return false;
    }


    public List<PlacementData> DataList
    {
        get
        {
            List<PlacementData> dataList = new List<PlacementData>();
            foreach (PlacementEntry entry in placements)
            {
                dataList.Add(entry.Data);
            }
            return dataList;
        }
    }




    /*
    //TODO move FileIO to different class move code to get + set?
    public List<PlacementEntry> Load(string filePath)
    {
        this.filePath = filePath;
        Save();
        placements = ServiceLocator.FileIO.ReadListFromFile<PlacementEntry>(filePath);
        PopulateCollisionGrid();
        return placements;

    }

    public void Save()
    {
        if (filePath != null)
        {
            ServiceLocator.FileIO.WriteListToFile(placements, filePath);
        }
    }*/

    

}

