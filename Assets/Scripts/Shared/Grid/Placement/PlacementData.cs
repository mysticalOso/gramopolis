﻿using System;

[Serializable]
public class PlacementData
{
    int id;
    int x;
    int y;

    public PlacementData(int id, int x, int y)
    {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public bool IsEqualValueTo(PlacementData e)
    {
        return (e.Id == id && e.X == x && e.Y == y);
    }

    public int Id { get { return id; } }
    public int X { get { return x; } }
    public int Y { get { return y; } }
}

