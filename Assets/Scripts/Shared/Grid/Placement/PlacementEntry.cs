﻿using System;
using UnityEngine;

public class PlacementEntry
{
    int id;
    int x;
    int y;
    int size;
    object linkedObject;

    public PlacementEntry(int id, int x, int y, int size, object linkedObject)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        this.size = size;
        this.linkedObject = linkedObject;
    }



    public bool IsTouching(Vector2 pos)
    {
        return (pos.x >= x && pos.x < (x+size) && pos.y >= y && pos.y < (y+size));
    }


    public PlacementData Data
    {
        get
        {
            return new PlacementData(id, x, y);
        }
    }


    public int Id { get { return id; } }
    public int X { get { return x; } }
    public int Y { get { return y; } }
    public int Size { get { return size; } }
    public object LinkedObject { get { return linkedObject; } }
}

