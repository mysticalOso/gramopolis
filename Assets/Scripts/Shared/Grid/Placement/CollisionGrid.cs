﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

//allows quick testing for various size square placements
public class CollisionGrid
{
    bool[,] collisionArray;

    public CollisionGrid(int xSize, int ySize)
    {
        collisionArray = new bool[xSize, ySize];
    }

    public bool Place(int x, int y, int size)
    {
        if (CanPlace(x, y, size))
        {
            for (int i = x; i < x + size; i++)
            {
                for (int j = y; j < y + size; j++)
                {
                    collisionArray[i, j] = true;
                }
            }
            return true;
        }
        return false;
    }

    public bool CanPlace(int x, int y, int size)
    {
        if (IsInRange(x,y, size))
        {
            for (int i = x; i < x + size; i++)
            {
                for (int j = y; j < y + size; j++)
                {
                    if (collisionArray[i, j]) return false;
                }
            }
            return true;
        }
        return false;
    }

    bool IsInRange(int x, int y, int size)
    {
        if (x>=0 && (x+size) <= collisionArray.GetLength(0))
        {
            return (y >= 0 && (y + size) <= collisionArray.GetLength(1));
        }
        return false;
    }

}

