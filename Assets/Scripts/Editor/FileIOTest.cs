﻿using NUnit.Framework;
using System.Collections.Generic;

[TestFixture]
public class FileIOTest
{
    FileIO fileIO;
    PlacementData a;
    PlacementData b;

    [TestFixtureSetUp]
    public void TestSetup()
    {
        List<PlacementData> list = new List<PlacementData>();
        a = new PlacementEntry(560,23,83,2,null).Data;
        b = new PlacementEntry(21, 1234, 4332,2,null).Data;
        list.Add(a);
        list.Add(b);

        fileIO = new FileIO();
        fileIO.WriteListToFile(list, "test.dat");
    }

    [Test]
    public void PlacementDataReadTest()
    {
        List<PlacementData> list = fileIO.ReadListFromFile<PlacementData>("test.dat");
        int index = 0;
        PlacementData c = null;
        PlacementData d = null;
        foreach (PlacementData entry in list)
        {
            if (index == 0) c = entry;
            if (index == 1) d = entry;
            index++;
        }
        Assert.True(a.IsEqualValueTo(c));
        Assert.True(b.IsEqualValueTo(d));
    }
}
