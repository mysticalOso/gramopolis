﻿using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
using NUnit.Framework;

[TestFixture]
public class PlacementGridTest
{
    PlacementGrid grid;

    [TestFixtureSetUp]
    public void TestSetup()
    {
        grid = new PlacementGrid(10, 20);
        grid.Place(2, 2, 1, 2, null);
        grid.Place(8, 2, 2, 1, null);
        grid.Place(5, 15, 3, 4, null);
    }

    [Test]
    public void SuccessfulPlacement()
    {
        Assert.True(grid.Place(5, 5, 4, 2, null));
        Assert.True(grid.Place(0, 11, 5, 4, null));
    }

    [Test]
    public void PlacementCollision()
    {
        Assert.False(grid.Place(1, 1, 6, 2, null));
        Assert.False(grid.Place(8, 18, 7, 1, null));
    }

    [Test]
    public void OutOfBounds()
    {
        Assert.False(grid.Place(9, 15, 8, 3, null));
        Assert.False(grid.Place(1, 18, 9, 4, null));

        Assert.False(grid.Place(-1, 0, 10, 1, null));
    }

    [Test]
    public void IdAlreadyPlaced()
    {
        Assert.True(grid.IsPlaced(1));
        Assert.True(grid.IsPlaced(3));
        Assert.False(grid.IsPlaced(11));
    }
}
